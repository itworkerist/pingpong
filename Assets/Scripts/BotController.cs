﻿using UnityEngine;

namespace PingPong
{
    public class BotController : IPaddleController
    {
        private const float MinAgility = 2f;
        
        private readonly Paddle _paddle;
        private readonly Ball _ball;
        private float _agility = MinAgility;

        public BotController(Paddle paddle, Ball ball)
        {
            _paddle = paddle;
            _ball = ball;
            Playfield.ScoreChanged += RecalculateAgility;
        }
        
        public void Dispose() => Playfield.ScoreChanged -= RecalculateAgility;

        public void Restart() => _paddle.Restart();

        public void Tick() => _paddle.MoveTo(_paddle.Position.x + (_ball.Position.x - _paddle.Position.x) * Mathf.Clamp01(Time.deltaTime * _agility));

        private void RecalculateAgility(int playerScore, int botScore) => _agility = MinAgility + playerScore / (botScore + 1f);
    }
}
