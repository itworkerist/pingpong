﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PingPong
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider2D))]
    public class Playfield : MonoBehaviour
    {
        [SerializeField] [Range(0.5f, 1f)] private float _ballMinSize;
        [SerializeField] [Range(1, 2)] private float _ballMaxSize;
        [SerializeField] private float _ballMinSpeed;
        [SerializeField] private float _ballMaxSpeed;
        [SerializeField] [Range(0, 30)] private float _ballDeltaSpawnAngle;
        [SerializeField] private Ball _ball;
        [SerializeField] private Paddle _topPaddle;
        [SerializeField] private Paddle _bottomPaddle;
        [SerializeField] private BoxCollider2D _leftBorder;
        [SerializeField] private BoxCollider2D _rightBorder;
        [SerializeField] private BoxCollider2D _topTrigger;
        [SerializeField] private BoxCollider2D _bottomTrigger;
        
        private int _playerScore;
        private int _opponentScore;
        private IPaddleController _playerController;
        private IPaddleController _opponentController;

        public static event Action<int, int> ScoreChanged;

        protected void Awake()
        {
            Application.targetFrameRate = 60;
            _playerController = new PlayerController(_bottomPaddle, _leftBorder.bounds.max.x, _rightBorder.bounds.min.x);
            _opponentController = new BotController(_topPaddle, _ball);
        }

        protected void Start() => Restart(0, 0);
        
        protected void OnTriggerExit2D(Collider2D other)
        {
            if (!other.CompareTag(Ball.Tag))
                return;

            float y = _ball.Position.y;
            if (_topTrigger.offset.y < y)
                Restart(_playerScore + 1, _opponentScore);
            else if (_bottomTrigger.offset.y > y)
                Restart(_playerScore, _opponentScore + 1);
        }

        protected void Update()
        {
            _playerController.Tick();
            _opponentController.Tick();
        }

        protected void OnDestroy()
        {
            _playerController.Dispose();
            _opponentController.Dispose();
        }

        private void SetScore(int playerScore, int opponentScore)
        {
            _playerScore = playerScore;
            _opponentScore = opponentScore;
            SaveBestScoreIfNeeded();
            ScoreChanged?.Invoke(_playerScore, _opponentScore);
        }

        private void SaveBestScoreIfNeeded()
        {
            if (Prefs.BestPlayerScore - Prefs.BestOpponentScore >= _playerScore - _opponentScore)
                return;
            
            Prefs.BestPlayerScore = _playerScore;
            Prefs.BestOpponentScore = _opponentScore;
            Prefs.Save();
        }
        
        private void Restart(int playerScore, int opponentScore)
        {
            float size = Random.Range(_ballMinSize, _ballMaxSize);
            Vector2 direction = Random.Range(-1, 1) >= 0 ? Vector2.up : Vector2.down;
            direction = Quaternion.Euler(0, 0, Random.Range(-_ballDeltaSpawnAngle, _ballDeltaSpawnAngle)) * direction;
            Vector2 velocity =  direction.normalized * Random.Range(_ballMinSpeed, _ballMaxSpeed);
            _ball.Restart(size, velocity);
            
            _playerController.Restart();
            _opponentController.Restart();
            SetScore(playerScore, opponentScore);
        }

        public void OnRestartClick() => Restart(0, 0);
    }
}
