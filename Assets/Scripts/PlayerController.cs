﻿using UnityEngine;

namespace PingPong
{
    public class PlayerController : IPaddleController
    {
        private readonly Paddle _paddle;
        private readonly float _paddleMinX;
        private readonly float _paddleMaxX;
        private readonly Camera _camera;
        private int _touchFingerId;
        private float _touchX;

        public PlayerController(Paddle paddle, float minX, float maxX)
        {
            _paddle = paddle;
            _paddleMinX = minX;
            _paddleMaxX = maxX;
            _camera = Camera.main;
            _touchFingerId = -1;
            Input.multiTouchEnabled = false;
        }

        public void Dispose() {}

        public void Restart() => _paddle.Restart();

        public void Tick()
        {
            if (Input.touchSupported)
            {
                if (Input.touchCount <= 0)
                    return;

                Touch touch = Input.GetTouch(0);
                HandleTouch(touch.fingerId, touch.phase, new Vector3(touch.position.x, touch.position.y, 0));
                
                return;
            }
            
            if (Input.GetMouseButtonDown(0))
                HandleTouch(0, TouchPhase.Began, Input.mousePosition);
            else if (Input.GetMouseButton(0))
                HandleTouch(0, TouchPhase.Moved, Input.mousePosition);
            else if (Input.GetMouseButtonUp(0))
                HandleTouch(0, TouchPhase.Ended, Input.mousePosition);
        }

        private void HandleTouch(int touchFingerId, TouchPhase touchPhase, Vector3 touchPosition)
        {
            switch (touchPhase)
            {
            case TouchPhase.Began:
                _touchFingerId = touchFingerId;
                _touchX = _camera.ScreenToWorldPoint(touchPosition).x;
                break;
            case TouchPhase.Moved:
                if (_touchFingerId == touchFingerId)
                {
                    float touchX = _camera.ScreenToWorldPoint(touchPosition).x;
                    _paddle.MoveTo(Mathf.Clamp(_paddle.Position.x + touchX - _touchX, _paddleMinX, _paddleMaxX));
                    _touchX = touchX;
                }
                break;
            case TouchPhase.Canceled:
            case TouchPhase.Ended:
                _touchFingerId = -1;
                break;
            }
        }
    }
}
