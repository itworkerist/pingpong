﻿using UnityEngine;
using UnityEngine.UI;

namespace PingPong
{
    [DisallowMultipleComponent]
    public class UIPresenter : MonoBehaviour
    {
        [SerializeField] private Text _scoreText;

        protected void OnEnable() => Playfield.ScoreChanged += UpdateScore;

        protected void OnDisable() => Playfield.ScoreChanged -= UpdateScore;

        private void UpdateScore(int playerScore, int opponentScore) => _scoreText.text = $"{playerScore} : {opponentScore} ({Prefs.BestPlayerScore} : {Prefs.BestOpponentScore})";
    }
}
