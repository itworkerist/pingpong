﻿using UnityEngine;

namespace PingPong
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BoxCollider2D))]
    public class PlayfieldBorder : MonoBehaviour
    {
        [SerializeField] private float _force;
        
        protected void OnCollisionExit2D(Collision2D collision)
        {
            Transform trans = collision.transform;
            
            if (!trans.CompareTag(Ball.Tag))
                return;
            
            var ball = trans.GetComponent<Ball>();
            ball.Impact((ball.Velocity.y < 0 ? Vector2.down : Vector2.up) * _force);
        }
    }
}
