﻿using UnityEngine;

namespace PingPong
{
    [DisallowMultipleComponent]
    public class Paddle : MonoBehaviour
    {
        private Transform _transform;
        private Vector3 _startPosition;

        public Vector2 Position => _transform.position;

        protected void Awake()
        {
            _transform = transform;
            _startPosition = _transform.position;
        }

        public void Restart() => _transform.position = _startPosition;

        public void MoveTo(float x)
        {
            Vector3 position = _transform.position;
            position.x = x;
            _transform.position = position;
        }
    }
}
