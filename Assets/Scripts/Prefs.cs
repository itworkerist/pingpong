﻿using UnityEngine;

namespace PingPong
{
    public static class Prefs
    {
        private const string BestPlayerScoreKey = "BestPlayerScoreKey";
        private const string BestOpponentScoreKey = "BestOpponentScoreKey";
        
        public static int BestPlayerScore
        {
            get => PlayerPrefs.GetInt(BestPlayerScoreKey, 0);
            set => PlayerPrefs.SetInt(BestPlayerScoreKey, value);
        }
        
        public static int BestOpponentScore
        {
            get => PlayerPrefs.GetInt(BestOpponentScoreKey, 0);
            set => PlayerPrefs.SetInt(BestOpponentScoreKey, value);
        }

        public static void Save() => PlayerPrefs.Save();
    }
}
