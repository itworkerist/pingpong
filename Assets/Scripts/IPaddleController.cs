﻿using System;

namespace PingPong
{
    public interface IPaddleController : IDisposable
    {
        void Restart();
        
        void Tick();
    }
}
