﻿using UnityEngine;

namespace PingPong
{
    [DisallowMultipleComponent]
    public class Ball : MonoBehaviour
    {
        public const string Tag = "Ball";
        
        [SerializeField] private Rigidbody2D _rigidbody;

        private Vector3 _startPosition;
        private float _sqrSpeed;

        public Vector2 Position => _rigidbody.position;

        public Vector2 Velocity => _rigidbody.velocity;

        protected void Awake() => _startPosition = transform.position;

        protected void Update()
        {
            if ((int)Velocity.sqrMagnitude < (int)_sqrSpeed)
                Impact(Velocity.normalized);
        }

        public void Restart(float size, Vector2 velocity)
        {
            _rigidbody.simulated = false;
            _rigidbody.isKinematic = true;
            transform.position = _startPosition;
            transform.localScale = Vector3.one * size;
            _rigidbody.velocity = velocity;
            _rigidbody.isKinematic = false;
            _rigidbody.simulated = true;

            _sqrSpeed = velocity.sqrMagnitude;
        }

        public void Impact(Vector2 force) => _rigidbody.AddForce(force);
    }
}
